<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ContactFormTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testExample()
    {
        //$this->assertTrue(true);
        $response = $this->json('POST', '/api/contactForm',
            [
                'name' => 'Sally',
                'email' => 'Sally@mail.com',
                'subject' => 'Sally',
                'message' => 'Sally',
                ]);

        $response
            ->assertStatus(200)
            ->assertJson([
                'status' => 'success',
            ]);
    }
}
