

function postContactForm() {

    //let submitBtn = document.getElementById('submitContactForm');
    let contactSection = document.getElementById('contact-section');
    let submitForm = document.getElementById('contactForm');
    let contactFormSuccess = document.getElementById('contactFormSuccess');
    let contactFormError = document.getElementById('contactFormError');
    const url = submitForm.getAttribute("action");

    // The data we are going to send in our request
    /*let formData = {
        name: cName,
    };*/

    // The parameters we are gonna pass to the fetch function
    let fetchData = {
        method: 'POST',
        credentials: "same-origin",
        body: new FormData(submitForm),
        //headers: new Headers()
        headers: {
            //"Content-Type": "application/json",
            "Accept": "application/json",
            "X-Requested-With": "XMLHttpRequest",
            "X-CSRF-Token": $('input[name="_token"]').val()
        },
    };

    fetch(url, fetchData)
        .then(function(response) {
            return response.json();
        })
        .then(function(data) {

            if( data.status === "success" ) {

                submitForm.reset();

                contactFormError.classList.add("hide");
                contactFormSuccess.classList.remove("hide");
                contactFormSuccess.innerText = data.message ;
            } else {

                contactFormSuccess.classList.add("hide");
                contactFormError.classList.remove("hide");
                contactFormError.innerText = data.message ;
            }
            contactSection.scrollIntoView();
            return;
        })
        .catch(function(error) {
            console.log(error);
        });

    return false;
}
