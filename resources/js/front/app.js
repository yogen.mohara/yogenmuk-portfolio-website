import Vue from 'vue';
import axios from 'axios';
import VueAxios from 'vue-axios';

import 'vuetify/dist/vuetify.min.css'

import Routes from '@/js/front/routes/routes.js';
import App from '@/js/front/views/App';

window.axios = axios;
Vue.use(VueAxios,axios);

window.axios.defaults.headers.common = {
    'X-Requested-With': 'XMLHttpRequest',
    'X-CSRF-TOKEN' : document.querySelector('meta[name="csrf-token"]').getAttribute('content')
};

Vue.config.devtools = true;
Vue.config.productionTip = false;

const app = new Vue({
    el: '#app',
    router: Routes,
    components: { App }
});

export default app;

