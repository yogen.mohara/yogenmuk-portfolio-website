import Vue from 'vue';
import VueRouter from 'vue-router';

import Home from '@/js/front/components/Home';
import About from '@/js/front/components/About';
import Resume from '@/js/front/components/Resume';
import Projects from '@/js/front/components/Projects';
import Contact from '@/js/front/components/Contact';

Vue.use(VueRouter);

export default new VueRouter({

    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'Home',
            component: Home
        },
        {
            path: '/about',
            name: 'About',
            component: About
        },
        {
            path: '/resume',
            name: 'Resume',
            component: Resume
        },
        {
            path: '/projects',
            name: 'Projects',
            component: Projects
        },
        {
            path: '/contact',
            name: 'Contact',
            component: Contact
        }
    ]
});
