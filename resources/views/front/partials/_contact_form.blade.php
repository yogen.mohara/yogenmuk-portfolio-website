<section class="ftco-section contact-section ftco-no-pb" id="contact-section">
    <div class="container">
        <div class="row justify-content-center mb-5 pb-3">
            <div class="col-md-7 heading-section text-center ftco-animate">
                <h1 class="big big-2">Contact</h1>
                <h2 class="mb-4">Contact Me</h2>
                <!--p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia</p>-->
                @if(Session::has('success'))
                    <div class="alert alert-success">
                        {{ Session::get('success') }}
                    </div>
                @endif
                <div id="contactFormSuccess" class="alert alert-success hide"></div>
                <div id="contactFormError" class="alert alert-error hide"></div>
            </div>
        </div>

        <!--<div class="row d-flex contact-info mb-5">
          <div class="col-md-6 col-lg-3 d-flex ftco-animate">
          	<div class="align-self-stretch box text-center p-4 shadow">
          		<div class="icon d-flex align-items-center justify-content-center">
          			<span class="icon-map-signs"></span>
          		</div>
          		<div>
	          		<h3 class="mb-4">Address</h3>
		            <p>198 West 21th Street, Suite 721 New York NY 10016</p>
		          </div>
	          </div>
          </div>
          <div class="col-md-6 col-lg-3 d-flex ftco-animate">
          	<div class="align-self-stretch box text-center p-4 shadow">
          		<div class="icon d-flex align-items-center justify-content-center">
          			<span class="icon-phone2"></span>
          		</div>
          		<div>
	          		<h3 class="mb-4">Contact Number</h3>
		            <p><a href="tel://1234567920">+ 1235 2355 98</a></p>
	            </div>
	          </div>
          </div>
          <div class="col-md-6 col-lg-3 d-flex ftco-animate">
          	<div class="align-self-stretch box text-center p-4 shadow">
          		<div class="icon d-flex align-items-center justify-content-center">
          			<span class="icon-paper-plane"></span>
          		</div>
          		<div>
	          		<h3 class="mb-4">Email Address</h3>
		            <p><a href="mailto:info@yoursite.com">info@yoursite.com</a></p>
		          </div>
	          </div>
          </div>
          <div class="col-md-6 col-lg-3 d-flex ftco-animate">
          	<div class="align-self-stretch box text-center p-4 shadow">
          		<div class="icon d-flex align-items-center justify-content-center">
          			<span class="icon-globe"></span>
          		</div>
          		<div>
	          		<h3 class="mb-4">Website</h3>
		            <p><a href="#">yoursite.com</a></p>
	            </div>
	          </div>
          </div>
        </div>-->

        <div class="row no-gutters block-9 mb-1">
            <div class="col-md-6 order-md-last d-flex">
                <form id="contactForm" method="post" action="{{ url('/api/contactForm') }}" class="bg-light p-4 p-md-5 contact-form"
                    onsubmit="return postContactForm();">
                    @csrf
                    <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                        <input type="text" class="form-control" placeholder="Your Name" name="name" value="{{old('name')}}">
                        <span class="text-danger">{{ $errors->first('name') }}</span>
                    </div>
                    <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                        <input type="text" class="form-control" placeholder="Your Email" name="email" value="{{old('email')}}">
                        <span class="text-danger">{{ $errors->first('email') }}</span>
                    </div>
                    <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                        <input type="text" class="form-control" placeholder="Subject" name="subject" value="{{old('subject')}}">
                        <span class="text-danger">{{ $errors->first('subject') }}</span>
                    </div>
                    <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                        <textarea id="" cols="30" rows="7" class="form-control" placeholder="Message" name="message">{{old('message')}}</textarea>
                        <span class="text-danger">{{ $errors->first('message') }}</span>
                    </div>
                    <div class="g-recaptcha"
                         data-sitekey="{{config('app.GOOGLE_RECAPTCHA_KEY')}}">
                    </div>
                    <div class="form-group mt-2">
                        <input type="submit" id="submitContactForm" value="Send Message" class="btn btn-primary py-3 px-5">
                    </div>
                </form>

            </div>

            <div class="col-md-6 d-flex">
                <div class="img" style="background-image: url(images/contact.jpg);"></div>
            </div>
        </div>
    </div>
</section>
