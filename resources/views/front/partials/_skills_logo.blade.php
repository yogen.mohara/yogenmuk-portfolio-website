<section class="ftco-section ftco-partner">
    <div class="container">
        <div class="row">
            <div class="col-sm ftco-animate">
                <a href="#" class="partner"><i class="devicon-php-plain colored"></i></a>
            </div>
            <div class="col-sm ftco-animate">
                <a href="#" class="partner"><i class="devicon-mysql-plain-wordmark colored"></i></a>
            </div>
            <div class="col-sm ftco-animate">
                <a href="#" class="partner"><i class="devicon-javascript-plain colored"></i></a>
            </div>
            <div class="col-sm ftco-animate">
                <a href="#" class="partner"><i class="devicon-html5-plain-wordmark colored"></i></a>
            </div>
            <div class="col-sm ftco-animate">
                <a href="#" class="partner"><i class="devicon-css3-plain-wordmark colored"></i></a>
            </div>
            <div class="col-sm ftco-animate">
                <a href="#" class="partner"><i class="devicon-bootstrap-plain-wordmark colored"></i></a>
            </div>
            <div class="col-sm ftco-animate">
                <a href="#" class="partner"><i class="devicon-codeigniter-plain-wordmark colored"></i></a>
            </div>
            {{--<div class="col-sm ftco-animate">
                <a href="#" class="partner"><i class="devicon-laravel-plain-wordmark colored"></i></a>
            </div>
            <div class="col-sm ftco-animate">
                <a href="#" class="partner"><i class="devicon-amazonwebservices-plain-wordmark colored"></i></a>
            </div>--}}
        </div>
    </div>
</section>
