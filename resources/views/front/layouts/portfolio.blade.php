<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <title>Yogendra Mohara - PHP Developer</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900" rel="stylesheet">

    <link rel="stylesheet" href="https://cdn.rawgit.com/konpa/devicon/df6431e323547add1b4cf45992913f15286456d3/devicon.min.css">

    <link rel="stylesheet" href="css/front/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="css/front/animate.css">

    <link rel="stylesheet" href="css/front/owl.carousel.min.css">
    <link rel="stylesheet" href="css/front/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/front/magnific-popup.css">

    <link rel="stylesheet" href="css/front/aos.css">

    <link rel="stylesheet" href="css/front/ionicons.min.css">

    <link rel="stylesheet" href="css/front/flaticon.css">
    <link rel="stylesheet" href="css/front/icomoon.css">
    <link rel="stylesheet" href="css/front/style.css?<?php echo time(); ?>">
</head>
<body data-spy="scroll" data-target=".site-navbar-target" data-offset="300">


@include( 'front.partials._header' )

@include( 'front.partials._banner' )

@include( 'front.partials._about' )

@include( 'front.partials._skills_logo' )

@include( 'front.partials._resume' )

@include( 'front.partials._services' )

@include( 'front.partials._about' )

@include( 'front.partials._hire_me' )

@include( 'front.partials._contact_form' )

<script src="js/front/jquery.min.js"></script>
<script src="js/front/jquery-migrate-3.0.1.min.js"></script>
<script src="js/front/popper.min.js"></script>
<script src="js/front/bootstrap.min.js"></script>
<script src="js/front/jquery.easing.1.3.js"></script>
<script src="js/front/jquery.waypoints.min.js"></script>
<script src="js/front/jquery.stellar.min.js"></script>
<script src="js/front/owl.carousel.min.js"></script>
<script src="js/front/jquery.magnific-popup.min.js"></script>
<script src="js/front/aos.js"></script>
<script src="js/front/jquery.animateNumber.min.js"></script>
<script src="js/front/scrollax.min.js"></script>

<script src="js/front/main.js"></script>
<script src="js/front/yogenmohara.js"></script>

<script src='https://www.google.com/recaptcha/api.js'></script>
</body>
</html>
