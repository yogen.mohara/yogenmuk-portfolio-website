<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <title>Yogendra Mohara - PHP Developer</title>
    <meta name="csrf-token" id="meta-csrf-token" content="{{ csrf_token() }}">
    <meta name="g-recaptcha" id="meta-g-recaptcha" content="{{config('app.GOOGLE_RECAPTCHA_KEY')}}">
    <meta name="base-url" content="{{ url('/') }}">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:100,200,300,400,500,600,700,800,900"
          rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/@mdi/font@4.x/css/materialdesignicons.min.css" rel="stylesheet">
    <link rel="stylesheet"
          href="https://cdn.rawgit.com/konpa/devicon/df6431e323547add1b4cf45992913f15286456d3/devicon.min.css">
    <link rel="stylesheet" href="assets/css/front/vendors.css">
    <link rel="stylesheet" href="assets/css/front/style.css?<?php echo time(); ?>">
</head>
<body data-spy="scroll" data-target=".site-navbar-target" data-offset="300">
<div id="app">
    <app></app>
</div>
<script src="{{ asset('assets/js/front/vendors.js') }}"></script>
<script src="{{ asset('assets/js/front/app.js') }}"></script>
<script src='https://www.google.com/recaptcha/api.js?onload=vueRecaptchaApiLoaded&render=explicit' async defer></script>
</body>
</html>
