<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <title>Yogendra Mohara - PHP Developer</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900" rel="stylesheet">

    <link rel="stylesheet" href="https://cdn.rawgit.com/konpa/devicon/df6431e323547add1b4cf45992913f15286456d3/devicon.min.css">

    <link rel="stylesheet" href="css/front/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="css/front/animate.css">

    <link rel="stylesheet" href="css/front/owl.carousel.min.css">
    <link rel="stylesheet" href="css/front/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/front/magnific-popup.css">

    <link rel="stylesheet" href="css/front/aos.css">

    <link rel="stylesheet" href="css/front/ionicons.min.css">

    <link rel="stylesheet" href="css/front/flaticon.css">
    <link rel="stylesheet" href="css/front/icomoon.css">
    <link rel="stylesheet" href="css/front/style.css?<?php echo time(); ?>">
</head>
<body data-spy="scroll" data-target=".site-navbar-target" data-offset="300">


<nav class="navbar navbar-expand-lg navbar-dark ftco_navbar ftco-navbar-light site-navbar-target" id="ftco-navbar">
    <div class="container">
        <!--<a class="navbar-brand" href="index.html"><span>Y</span>ogenMohara</a>
        <a class="navbar-brand" href="index.html">YogenMohara</a>-->
        <button class="navbar-toggler js-fh5co-nav-toggle fh5co-nav-toggle" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="oi oi-menu"></span> Menu
        </button>

        <div class="collapse navbar-collapse" id="ftco-nav">
            <!--<ul class="navbar-nav nav ml-auto">-->
            <ul class="navbar-nav nav m-auto">
                <li class="nav-item"><a href="#home-section" class="nav-link"><span>Home</span></a></li>
                <li class="nav-item"><a href="#about-section" class="nav-link"><span>About</span></a></li>
                <li class="nav-item"><a href="#resume-section" class="nav-link"><span>Resume</span></a></li>
                <li class="nav-item"><a href="#services-section" class="nav-link"><span>Services</span></a></li>
                {{--<li class="nav-item"><a href="#projects-section" class="nav-link"><span>Projects</span></a></li>--}}
                {{--<li class="nav-item"><a href="#blog-section" class="nav-link"><span>My Blog</span></a></li>--}}
                <li class="nav-item"><a href="#contact-section" class="nav-link"><span>Contact</span></a></li>
            </ul>
        </div>
    </div>
</nav>

<section class="hero-wrap js-fullheight">
    <div class="overlay"></div>
    <div class="container">
        <div class="row no-gutters slider-text js-fullheight justify-content-center align-items-center">
            <div class="col-lg-8 col-md-6 ftco-animate d-flex align-items-center">
                <div class="text text-center">
                    <span class="subheading">Hello! I am</span>
                    <h1>Yogendra Mohara</h1>
                    <h2>I'm a
                        <span
                            class="txt-rotate"
                            data-period="2000"
                            data-rotate='[ "PHP Developer.", "Web Developer.", "Codeigniter Developer." ]'></span>
                    </h2>
                </div>
            </div>
        </div>
    </div>
    <div class="mouse">
        <a href="#" class="mouse-icon">
            <div class="mouse-wheel"><span class="ion-ios-arrow-round-down"></span></div>
        </a>
    </div>
</section>

<section class="ftco-about img ftco-section ftco-no-pt ftco-no-pb" id="about-section">
    <div class="container">
        <div class="row d-flex no-gutters">
            <div class="col-md-6 col-lg-6 d-flex">
                <div class="img-about img d-flex align-items-stretch">
                    <div class="overlay"></div>
                    <div class="img d-flex align-self-stretch align-items-center" style="background-image:url(images/profile-photo-2.jpeg);background-position-y: 0;">
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-6 pl-md-5 py-5">
                <div class="row justify-content-start pb-3">
                    <div class="col-md-12 heading-section ftco-animate">
                        <h1 class="big">About</h1>
                        <h2 class="mb-4">About Me</h2>
                        <p>I have 10+ years of experience in developing websites and web applications.
                            I specialize in developing back-end applications with PHP.</p>
                        <ul class="about-info mt-4 px-md-0 px-2">
                            <li class="d-flex"><span>Name:</span> <span>Yogendra Mohara</span></li>
                            <li class="d-flex"><span>Born:</span> <span>1987</span></li>
                            <li class="d-flex"><span>Location:</span> <span>Basingstoke, United Kingdom</span></li>
                            {{--<li class="d-flex"><span>Email:</span> <span><a href="#contact-section">Send Message</a></span></li>--}}
                        </ul>
                    </div>
                </div>
                <div class="counter-wrap ftco-animate d-flex mt-md-3">
                    <div class="text">
                        {{--<p class="mb-4">
                            <span class="number" data-number="120">0</span>
                            <span>Project complete</span>
                        </p>--}}
                        <p><a href="#contact-section" class="btn btn-primary py-3 px-3">Hire Me</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="ftco-section ftco-partner">
    <div class="container">
        <div class="row">
            <div class="col-sm ftco-animate">
                <a href="#" class="partner"><i class="devicon-php-plain colored"></i></a>
            </div>
            <div class="col-sm ftco-animate">
                <a href="#" class="partner"><i class="devicon-mysql-plain-wordmark colored"></i></a>
            </div>
            <div class="col-sm ftco-animate">
                <a href="#" class="partner"><i class="devicon-javascript-plain colored"></i></a>
            </div>
            <div class="col-sm ftco-animate">
                <a href="#" class="partner"><i class="devicon-html5-plain-wordmark colored"></i></a>
            </div>
            <div class="col-sm ftco-animate">
                <a href="#" class="partner"><i class="devicon-css3-plain-wordmark colored"></i></a>
            </div>
            <div class="col-sm ftco-animate">
                <a href="#" class="partner"><i class="devicon-bootstrap-plain-wordmark colored"></i></a>
            </div>
            <div class="col-sm ftco-animate">
                <a href="#" class="partner"><i class="devicon-codeigniter-plain-wordmark colored"></i></a>
            </div>
            {{--<div class="col-sm ftco-animate">
                <a href="#" class="partner"><i class="devicon-laravel-plain-wordmark colored"></i></a>
            </div>
            <div class="col-sm ftco-animate">
                <a href="#" class="partner"><i class="devicon-amazonwebservices-plain-wordmark colored"></i></a>
            </div>--}}
        </div>
    </div>
</section>

<section class="ftco-section ftco-no-pb goto-here" id="resume-section">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <nav id="navi">
                    <ul>
                        <li><a href="#page-1">Education</a></li>
                        <li><a href="#page-2">Experience</a></li>
                        <li><a href="#page-3">Skills</a></li>
                    </ul>
                </nav>
            </div>
            <div class="col-md-9">
                <div id="page-1" class= "page one">
                    <h2 class="heading">Education</h2>
                    <div class="resume-wrap d-flex ftco-animate">
                        <div class="icon d-flex align-items-center justify-content-center">
                            <span class="flaticon-ideas"></span>
                        </div>
                        <div class="text pl-3">
                            <span class="date">2008-2012</span>
                            <h2>Bachelor of Science in Computing ( 1st Class Honors )</h2>
                            <span class="position">Information College, Kathmandu, Nepal <br/>( University of Portsmouth, UK )</span>
                        </div>
                    </div>
                    <div class="resume-wrap d-flex ftco-animate">
                        <div class="icon d-flex align-items-center justify-content-center">
                            <span class="flaticon-ideas"></span>
                        </div>
                        <div class="text pl-3">
                            <span class="date">2003-2005</span>
                            <h2>GCE A Levels</h2>
                            <span class="position">Rai International School, New Delhi, India</span>
                        </div>
                    </div>
                </div>

                <div id="page-2" class= "page two">
                    <h2 class="heading">Experience</h2>
                    <div class="resume-wrap d-flex ftco-animate">
                        <div class="icon d-flex align-items-center justify-content-center">
                            <span class="flaticon-ideas"></span>
                        </div>
                        <div class="text pl-3">
                            <span class="date">2018-Present</span>
                            <h2>Full Stack PHP Developer</h2>
                            <span class="position">Freelance</span>
                        </div>
                    </div>
                    <div class="resume-wrap d-flex ftco-animate">
                        <div class="icon d-flex align-items-center justify-content-center">
                            <span class="flaticon-ideas"></span>
                        </div>
                        <div class="text pl-3">
                            <span class="date">2015-2018</span>
                            <h2>PHP Backend Developer</h2>
                            <span class="position">GIMO</span>
                        </div>
                    </div>
                    <div class="resume-wrap d-flex ftco-animate">
                        <div class="icon d-flex align-items-center justify-content-center">
                            <span class="flaticon-ideas"></span>
                        </div>
                        <div class="text pl-3">
                            <span class="date">2010-2015</span>
                            <h2>PHP Developer</h2>
                            <span class="position">Freelance</span>
                        </div>
                    </div>
                </div>
                <div id="page-3" class= "page three">
                    <h2 class="heading">Skills</h2>
                    {{--<div class="row progress-circle mb-5">
                        <div class="col-lg-3 mb-4">
                            <div class="bg-white rounded-lg shadow p-4">
                                <h2 class="h5 font-weight-bold text-center mb-4">PHP</h2>

                                <!-- Progress bar 1 -->
                                <div class="progress mx-auto" data-value='80'>
						          <span class="progress-left">
                        <span class="progress-bar border-primary"></span>
						          </span>
                                    <span class="progress-right">
                        <span class="progress-bar border-primary"></span>
						          </span>
                                    <div class="progress-value w-100 h-100 rounded-circle d-flex align-items-center justify-content-center">
                                        <div class="h2 font-weight-bold">80<sup class="small">%</sup></div>
                                    </div>
                                </div>
                                <!-- END -->
                            </div>
                        </div>

                        <div class="col-lg-3 mb-4">
                            <div class="bg-white rounded-lg shadow p-4">
                                <h2 class="h5 font-weight-bold text-center mb-4">MySQL</h2>

                                <!-- Progress bar 1 -->
                                <div class="progress mx-auto" data-value='80'>
						          <span class="progress-left">
                        <span class="progress-bar border-primary"></span>
						          </span>
                                    <span class="progress-right">
                        <span class="progress-bar border-primary"></span>
						          </span>
                                    <div class="progress-value w-100 h-100 rounded-circle d-flex align-items-center justify-content-center">
                                        <div class="h2 font-weight-bold">80<sup class="small">%</sup></div>
                                    </div>
                                </div>
                                <!-- END -->
                            </div>
                        </div>

                        <div class="col-lg-3 mb-4">
                            <div class="bg-white rounded-lg shadow p-4">
                                <h2 class="h5 font-weight-bold text-center mb-4">HTML</h2>

                                <!-- Progress bar 1 -->
                                <div class="progress mx-auto" data-value='75'>
						          <span class="progress-left">
                        <span class="progress-bar border-primary"></span>
						          </span>
                                    <span class="progress-right">
                        <span class="progress-bar border-primary"></span>
						          </span>
                                    <div class="progress-value w-100 h-100 rounded-circle d-flex align-items-center justify-content-center">
                                        <div class="h2 font-weight-bold">75<sup class="small">%</sup></div>
                                    </div>
                                </div>
                                <!-- END -->
                            </div>
                        </div>

                        <div class="col-lg-3 mb-4">
                            <div class="bg-white rounded-lg shadow p-4">
                                <h2 class="h5 font-weight-bold text-center mb-4">Javascript</h2>

                                <!-- Progress bar 1 -->
                                <div class="progress mx-auto" data-value='75'>
						          <span class="progress-left">
                        <span class="progress-bar border-primary"></span>
						          </span>
                                    <span class="progress-right">
                        <span class="progress-bar border-primary"></span>
						          </span>
                                    <div class="progress-value w-100 h-100 rounded-circle d-flex align-items-center justify-content-center">
                                        <div class="h2 font-weight-bold">75<sup class="small">%</sup></div>
                                    </div>
                                </div>
                                <!-- END -->
                            </div>
                        </div>
                    </div>--}}
                    <div class="row">
                        <div class="col-md-6 animate-box">
                            <div class="progress-wrap ftco-animate">
                                <h3>PHP</h3>
                                <div class="progress">
                                    <div class="progress-bar color-1" role="progressbar" aria-valuenow="80"
                                         aria-valuemin="0" aria-valuemax="100" style="width:80%">
                                        <span>80%</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 animate-box">
                            <div class="progress-wrap ftco-animate">
                                <h3>MySQL</h3>
                                <div class="progress">
                                    <div class="progress-bar color-1" role="progressbar" aria-valuenow="80"
                                         aria-valuemin="0" aria-valuemax="100" style="width:80%">
                                        <span>80%</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 animate-box">
                            <div class="progress-wrap ftco-animate">
                                <h3>HTML</h3>
                                <div class="progress">
                                    <div class="progress-bar color-1" role="progressbar" aria-valuenow="75"
                                         aria-valuemin="0" aria-valuemax="100" style="width:75%">
                                        <span>75%</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 animate-box">
                            <div class="progress-wrap ftco-animate">
                                <h3>Javascript</h3>
                                <div class="progress">
                                    <div class="progress-bar color-1" role="progressbar" aria-valuenow="75"
                                         aria-valuemin="0" aria-valuemax="100" style="width:75%">
                                        <span>75%</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 animate-box">
                            <div class="progress-wrap ftco-animate">
                                <h3>CodeIgniter</h3>
                                <div class="progress">
                                    <div class="progress-bar color-1" role="progressbar" aria-valuenow="80"
                                         aria-valuemin="0" aria-valuemax="100" style="width:80%">
                                        <span>80%</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 animate-box">
                            <div class="progress-wrap ftco-animate">
                                <h3>jQuery</h3>
                                <div class="progress">
                                    <div class="progress-bar color-2" role="progressbar" aria-valuenow="75"
                                         aria-valuemin="0" aria-valuemax="100" style="width:75%">
                                        <span>75%</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 animate-box">
                            <div class="progress-wrap ftco-animate">
                                <h3>CSS</h3>
                                <div class="progress">
                                    <div class="progress-bar color-4" role="progressbar" aria-valuenow="75"
                                         aria-valuemin="0" aria-valuemax="100" style="width:75%">
                                        <span>75%</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 animate-box">
                            <div class="progress-wrap ftco-animate">
                                <h3>SCSS</h3>
                                <div class="progress">
                                    <div class="progress-bar color-4" role="progressbar" aria-valuenow="75"
                                         aria-valuemin="0" aria-valuemax="100" style="width:75%">
                                        <span>75%</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 animate-box">
                            <div class="progress-wrap ftco-animate">
                                <h3>Laravel</h3>
                                <div class="progress">
                                    <div class="progress-bar color-5" role="progressbar" aria-valuenow="50"
                                         aria-valuemin="0" aria-valuemax="100" style="width:50%">
                                        <span>50%</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 animate-box">
                            <div class="progress-wrap ftco-animate">
                                <h3>AWS</h3>
                                <div class="progress">
                                    <div class="progress-bar color-6" role="progressbar" aria-valuenow="50"
                                         aria-valuemin="0" aria-valuemax="100" style="width:50%">
                                        <span>50%</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 animate-box">
                            <div class="progress-wrap ftco-animate">
                                <h3>LAMP Stack</h3>
                                <div class="progress">
                                    <div class="progress-bar color-6" role="progressbar" aria-valuenow="75"
                                         aria-valuemin="0" aria-valuemax="100" style="width:75%">
                                        <span>75%</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 animate-box">
                            <div class="progress-wrap ftco-animate">
                                <h3>LEMP Stack</h3>
                                <div class="progress">
                                    <div class="progress-bar color-6" role="progressbar" aria-valuenow="75"
                                         aria-valuemin="0" aria-valuemax="100" style="width:75%">
                                        <span>75%</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="ftco-section" id="services-section">
    <div class="container-fluid px-md-5">
        <div class="row justify-content-center py-5 mt-5">
            <div class="col-md-12 heading-section text-center ftco-animate">
                <h1 class="big big-2">Services</h1>
                {{--<h2 class="mb-4">Services</h2>
                <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia</p>--}}
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 text-center d-flex ftco-animate">
                <a href="#" class="services-1 shadow">
							<span class="icon">
								<i class="flaticon-analysis"></i>
							</span>
                    <div class="desc">
                        <h3 class="mb-5">Web Development</h3>
                        <!--<p>A small river named Duden flows by their place and supplies it with the necessary regelialia.</p>-->
                    </div>
                </a>
            </div>
            <div class="col-md-4 text-center d-flex ftco-animate">
                <a href="#" class="services-1 shadow">
							<span class="icon">
								<i class="flaticon-flasks"></i>
							</span>
                    <div class="desc">
                        <h3 class="mb-5">Database Development</h3>
                        <!--<p>A small river named Duden flows by their place and supplies it with the necessary regelialia.</p>-->
                    </div>
                </a>
            </div>
            <div class="col-md-4 text-center d-flex ftco-animate">
                <a href="#" class="services-1 shadow">
							<span class="icon">
								<i class="flaticon-ideas"></i>
							</span>
                    <div class="desc">
                        <h3 class="mb-5">CodeIgniter Development</h3>
                        <!--<p>A small river named Duden flows by their place and supplies it with the necessary regelialia.</p>-->
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>

<!--<section class="ftco-section ftco-project" id="projects-section">
    <div class="container-fluid px-md-0">
        <div class="row no-gutters justify-content-center pb-5">
      <div class="col-md-12 heading-section text-center ftco-animate">
          <h1 class="big big-2">Projects</h1>
        <h2 class="mb-4">Our Projects</h2>
        <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia</p>
      </div>
    </div>
        <div class="row no-gutters">
            <div class="col-md-4">
                <div class="project img ftco-animate d-flex justify-content-center align-items-center" style="background-image: url(images/work-1.jpg);">
                    <div class="overlay"></div>
                    <div class="text text-center p-4">
                        <h3><a href="#">Branding &amp; Illustration Design</a></h3>
                        <span>Web Design</span>
                    </div>
                </div>
              </div>
              <div class="col-md-4">
                <div class="project img ftco-animate d-flex justify-content-center align-items-center" style="background-image: url(images/work-2.jpg);">
                    <div class="overlay"></div>
                    <div class="text text-center p-4">
                        <h3><a href="#">Branding &amp; Illustration Design</a></h3>
                        <span>Web Design</span>
                    </div>
                </div>
              </div>

            <div class="col-md-4">
                <div class="project img ftco-animate d-flex justify-content-center align-items-center" style="background-image: url(images/work-3.jpg);">
                    <div class="overlay"></div>
                    <div class="text text-center p-4">
                        <h3><a href="#">Branding &amp; Illustration Design</a></h3>
                        <span>Web Design</span>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="project img ftco-animate d-flex justify-content-center align-items-center" style="background-image: url(images/work-4.jpg);">
                    <div class="overlay"></div>
                    <div class="text text-center p-4">
                        <h3><a href="#">Branding &amp; Illustration Design</a></h3>
                        <span>Web Design</span>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="project img ftco-animate d-flex justify-content-center align-items-center" style="background-image: url(images/work-5.jpg);">
                    <div class="overlay"></div>
                    <div class="text text-center p-4">
                        <h3><a href="#">Branding &amp; Illustration Design</a></h3>
                        <span>Web Design</span>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="project img ftco-animate d-flex justify-content-center align-items-center" style="background-image: url(images/work-6.jpg);">
                    <div class="overlay"></div>
                    <div class="text text-center p-4">
                        <h3><a href="#">Branding &amp; Illustration Design</a></h3>
                        <span>Web Design</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>-->

<!--<section class="ftco-section ftco-no-pt ftco-no-pb ftco-counter img" id="section-counter">
    <div class="container-fluid px-md-5">
            <div class="row d-md-flex align-items-center">
      <div class="col-md d-flex justify-content-center counter-wrap ftco-animate">
        <div class="block-18 shadow">
          <div class="text">
            <strong class="number" data-number="100">0</strong>
            <span>Awards</span>
          </div>
        </div>
      </div>
      <div class="col-md d-flex justify-content-center counter-wrap ftco-animate">
        <div class="block-18 shadow">
          <div class="text">
            <strong class="number" data-number="1200">0</strong>
            <span>Complete Projects</span>
          </div>
        </div>
      </div>
      <div class="col-md d-flex justify-content-center counter-wrap ftco-animate">
        <div class="block-18 shadow">
          <div class="text">
            <strong class="number" data-number="1200">0</strong>
            <span>Happy Customers</span>
          </div>
        </div>
      </div>
      <div class="col-md d-flex justify-content-center counter-wrap ftco-animate">
        <div class="block-18 shadow">
          <div class="text">
            <strong class="number" data-number="500">0</strong>
            <span>Cups of coffee</span>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>-->


<!--<section class="ftco-section" id="blog-section">
  <div class="container">
    <div class="row justify-content-center mb-5 pb-5">
      <div class="col-md-7 heading-section text-center ftco-animate">
        <h1 class="big big-2">Blog</h1>
        <h2 class="mb-4">Our Blog</h2>
        <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia</p>
      </div>
    </div>
    <div class="row d-flex">
      <div class="col-md-4 d-flex ftco-animate">
          <div class="blog-entry justify-content-end">
          <a href="single.html" class="block-20" style="background-image: url('images/image_1.jpg');">
          </a>
          <div class="text mt-3 float-right d-block">
            <h3 class="heading"><a href="single.html">Why Lead Generation is Key for Business Growth</a></h3>
            <div class="d-flex align-items-center mb-3 meta">
                <p class="mb-0">
                    <span class="mr-2">Sept. 12, 2019</span>
                    <a href="#" class="mr-2">Admin</a>
                    <a href="#" class="meta-chat"><span class="icon-chat"></span> 3</a>
                </p>
            </div>
            <p>A small river named Duden flows by their place and supplies it with the necessary regelialia.</p>
          </div>
        </div>
      </div>
      <div class="col-md-4 d-flex ftco-animate">
          <div class="blog-entry justify-content-end">
          <a href="single.html" class="block-20" style="background-image: url('images/image_2.jpg');">
          </a>
          <div class="text mt-3 float-right d-block">
            <h3 class="heading"><a href="single.html">Why Lead Generation is Key for Business Growth</a></h3>
            <div class="d-flex align-items-center mb-3 meta">
                <p class="mb-0">
                    <span class="mr-2">Sept. 12, 2019</span>
                    <a href="#" class="mr-2">Admin</a>
                    <a href="#" class="meta-chat"><span class="icon-chat"></span> 3</a>
                </p>
            </div>
            <p>A small river named Duden flows by their place and supplies it with the necessary regelialia.</p>
          </div>
        </div>
      </div>
      <div class="col-md-4 d-flex ftco-animate">
          <div class="blog-entry">
          <a href="single.html" class="block-20" style="background-image: url('images/image_3.jpg');">
          </a>
          <div class="text mt-3 float-right d-block">
            <h3 class="heading"><a href="single.html">Why Lead Generation is Key for Business Growth</a></h3>
            <div class="d-flex align-items-center mb-3 meta">
                <p class="mb-0">
                    <span class="mr-2">Sept. 12, 2019</span>
                    <a href="#" class="mr-2">Admin</a>
                    <a href="#" class="meta-chat"><span class="icon-chat"></span> 3</a>
                </p>
            </div>
            <p>A small river named Duden flows by their place and supplies it with the necessary regelialia.</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>-->

<section class="ftco-section ftco-hireme img" style="background-image: url(images/bg_1.jpg)">
    <div class="overlay"></div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-7 ftco-animate text-center">
                <h2>I'm <span>Available</span> for freelancing</h2>
                <!--<p>A small river named Duden flows by their place and supplies it with the necessary regelialia.</p>-->
                <p class="mb-0"><a href="#contact-section" class="btn btn-primary py-3 px-5">Hire me</a></p>
            </div>
        </div>
    </div>
</section>

<section class="ftco-section contact-section ftco-no-pb" id="contact-section">
    <div class="container">
        <div class="row justify-content-center mb-5 pb-3">
            <div class="col-md-7 heading-section text-center ftco-animate">
                <h1 class="big big-2">Contact</h1>
                <h2 class="mb-4">Contact Me</h2>
                <!--p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia</p>-->
                @if(Session::has('success'))
                    <div class="alert alert-success">
                        {{ Session::get('success') }}
                    </div>
                @endif
                <div id="contactFormSuccess" class="alert alert-success hide"></div>
                <div id="contactFormError" class="alert alert-error hide"></div>
            </div>
        </div>

        <!--<div class="row d-flex contact-info mb-5">
          <div class="col-md-6 col-lg-3 d-flex ftco-animate">
          	<div class="align-self-stretch box text-center p-4 shadow">
          		<div class="icon d-flex align-items-center justify-content-center">
          			<span class="icon-map-signs"></span>
          		</div>
          		<div>
	          		<h3 class="mb-4">Address</h3>
		            <p>198 West 21th Street, Suite 721 New York NY 10016</p>
		          </div>
	          </div>
          </div>
          <div class="col-md-6 col-lg-3 d-flex ftco-animate">
          	<div class="align-self-stretch box text-center p-4 shadow">
          		<div class="icon d-flex align-items-center justify-content-center">
          			<span class="icon-phone2"></span>
          		</div>
          		<div>
	          		<h3 class="mb-4">Contact Number</h3>
		            <p><a href="tel://1234567920">+ 1235 2355 98</a></p>
	            </div>
	          </div>
          </div>
          <div class="col-md-6 col-lg-3 d-flex ftco-animate">
          	<div class="align-self-stretch box text-center p-4 shadow">
          		<div class="icon d-flex align-items-center justify-content-center">
          			<span class="icon-paper-plane"></span>
          		</div>
          		<div>
	          		<h3 class="mb-4">Email Address</h3>
		            <p><a href="mailto:info@yoursite.com">info@yoursite.com</a></p>
		          </div>
	          </div>
          </div>
          <div class="col-md-6 col-lg-3 d-flex ftco-animate">
          	<div class="align-self-stretch box text-center p-4 shadow">
          		<div class="icon d-flex align-items-center justify-content-center">
          			<span class="icon-globe"></span>
          		</div>
          		<div>
	          		<h3 class="mb-4">Website</h3>
		            <p><a href="#">yoursite.com</a></p>
	            </div>
	          </div>
          </div>
        </div>-->

        <div class="row no-gutters block-9 mb-1">
            <div class="col-md-6 order-md-last d-flex">
                <form id="contactForm" method="post" action="{{ url('/api/contactForm') }}" class="bg-light p-4 p-md-5 contact-form"
                    onsubmit="return postContactForm();">
                    @csrf
                    <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                        <input type="text" class="form-control" placeholder="Your Name" name="name" value="{{old('name')}}">
                        <span class="text-danger">{{ $errors->first('name') }}</span>
                    </div>
                    <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                        <input type="text" class="form-control" placeholder="Your Email" name="email" value="{{old('email')}}">
                        <span class="text-danger">{{ $errors->first('email') }}</span>
                    </div>
                    <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                        <input type="text" class="form-control" placeholder="Subject" name="subject" value="{{old('subject')}}">
                        <span class="text-danger">{{ $errors->first('subject') }}</span>
                    </div>
                    <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                        <textarea id="" cols="30" rows="7" class="form-control" placeholder="Message" name="message">{{old('message')}}</textarea>
                        <span class="text-danger">{{ $errors->first('message') }}</span>
                    </div>
                    <div class="g-recaptcha"
                         data-sitekey="{{config('app.GOOGLE_RECAPTCHA_KEY')}}">
                    </div>
                    <div class="form-group mt-2">
                        <input type="submit" id="submitContactForm" value="Send Message" class="btn btn-primary py-3 px-5">
                    </div>
                </form>

            </div>

            <div class="col-md-6 d-flex">
                <div class="img" style="background-image: url(images/contact.jpg);"></div>
            </div>
        </div>
    </div>
</section>


<footer class="ftco-footer ftco-section">
    <div class="container">
        <div class="row mb-5">
            <div class="col-md">
                <div class="ftco-footer-widget mb-4">
                    <h2 class="ftco-heading-2">About</h2>
                    <p>
                        I have 10+ years of experience in developing websites and web applications.
                        I specialize in developing backend applications using PHP.</p>
                    {{--<ul class="ftco-footer-social list-unstyled float-md-left float-lft mt-5">
                        <li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
                        <li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li>
                        <li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
                    </ul>--}}
                </div>
            </div>
            <div class="col-md">
                <div class="ftco-footer-widget mb-4 ml-md-4">
                    <h2 class="ftco-heading-2">Links</h2>
                    <ul class="list-unstyled">
                        <li><a href="#home-section"><span class="icon-long-arrow-right mr-2"></span>Home</a></li>
                        <li><a href="#about-section"><span class="icon-long-arrow-right mr-2"></span>About</a></li>
                        <li><a href="#resume-section"><span class="icon-long-arrow-right mr-2"></span>Resume</a></li>
                        <li><a href="#services-section"><span class="icon-long-arrow-right mr-2"></span>Services</a></li>
                        {{--<li><a href="#"><span class="icon-long-arrow-right mr-2"></span>Projects</a></li>--}}
                        <li><a href="#contact-section"><span class="icon-long-arrow-right mr-2"></span>Contact</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md">
                <div class="ftco-footer-widget mb-4">
                    <h2 class="ftco-heading-2">Services</h2>
                    <ul class="list-unstyled">
                        <li><a href="#"><span class="icon-long-arrow-right mr-2"></span>Web Design</a></li>
                        <li><a href="#"><span class="icon-long-arrow-right mr-2"></span>Web Development</a></li>
                        <li><a href="#"><span class="icon-long-arrow-right mr-2"></span>Database Design</a></li>
                        <li><a href="#"><span class="icon-long-arrow-right mr-2"></span>Database Development</a></li>
                        <li><a href="#"><span class="icon-long-arrow-right mr-2"></span>Web Application Development</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md">
                <div class="ftco-footer-widget mb-4">
                    <h2 class="ftco-heading-2">Have a Question?</h2>
                    <div class="block-23 mb-3">
                        <ul>
                            {{--<li><span class="icon icon-map-marker"></span><span class="text">203 Fake St. Mountain View, San Francisco, California, USA</span></li>
                            <li><a href="#"><span class="icon icon-phone"></span><span class="text">+2 392 3929 210</span></a></li>--}}
                            <li><a href="#contact-section"><span class="icon icon-envelope"></span><span class="text">Send a message</span></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center">

                <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="icon-heart color-danger" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
            </div>
        </div>
    </div>
</footer>

<!-- loader -->
<div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>


<script src="js/front/jquery.min.js"></script>
<script src="js/front/jquery-migrate-3.0.1.min.js"></script>
<script src="js/front/popper.min.js"></script>
<script src="js/front/bootstrap.min.js"></script>
<script src="js/front/jquery.easing.1.3.js"></script>
<script src="js/front/jquery.waypoints.min.js"></script>
<script src="js/front/jquery.stellar.min.js"></script>
<script src="js/front/owl.carousel.min.js"></script>
<script src="js/front/jquery.magnific-popup.min.js"></script>
<script src="js/front/aos.js"></script>
<script src="js/front/jquery.animateNumber.min.js"></script>
<script src="js/front/scrollax.min.js"></script>

<script src="js/front/main.js"></script>
<script src="js/front/yogenmohara.js"></script>

<script src='https://www.google.com/recaptcha/api.js'></script>
</body>
</html>
