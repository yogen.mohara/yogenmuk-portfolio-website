<?php

namespace App\Mail;

use Illuminate\http\Request;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContactFormMessage extends Mailable
{
    use Queueable, SerializesModels;
    /**
     * @var Request
     */
    public $email;

    /**
     * Create a new message instance.
     *
     * @param Request $request
     */
    public function __construct( Request $request )
    {
        //
        $this -> email = $request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this -> subject( 'New Contact Form Message from yogenm.uk' )
            -> from( config('app.MAIL_FROM_ADDRESS'), 'No-reply' )
            -> to( config('app.MAIL_RECEPIENT') )
            ->view('mail.contact-form');
    }
}
