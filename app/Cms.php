<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cms extends Model
{
    //
    public $table = 'cms';
    public $fillable = ['category','key','value'];
}
