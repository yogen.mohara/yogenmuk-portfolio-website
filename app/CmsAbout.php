<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CmsAbout extends Model
{
    //
    public $table = 'cms_about';
    public $fillable = ['title','slug','is_published'];
}
