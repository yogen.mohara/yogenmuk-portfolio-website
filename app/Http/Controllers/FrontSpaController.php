<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FrontSpaController extends Controller
{
    public function index()
    {
        return view('front.layouts.spa_front');
    }
    public function cv()
    {
            return Storage::download('CV-YOGENDRA-MOHARA.pdf');
            
        /* $file = storage_path('files/CV-YOGENDRA-MOHARA.pdf');

        if (file_exists($file)) {

            $headers = [
                'Content-Type' => 'application/pdf'
            ];

            return response()->download($file, 'CV-YOGENDRA-MOHARA', $headers, 'inline');
        } else {
            abort(404, 'File not found!');
        } */
    }
}
