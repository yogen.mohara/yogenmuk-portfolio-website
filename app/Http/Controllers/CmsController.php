<?php

namespace App\Http\Controllers;

use App\Cms;
use App\CmsAbout;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CmsController extends Controller
{
    public function index()
    {
        $cms_data = Cms::all();
        $cms_data_by_key = [];
        foreach($cms_data as $data) {
            $cms_data_by_key[$data['key']] = $data['value'];
        }
        $response = [
            'about' => $cms_data_by_key,
            'education' => [
                [
                    'time_period' => '2008-2012',
                    'course' => 'Bachelor of Science in Computing ( 1st Class Honors )',
                    'institution' => 'Information College, Kathmandu, Nepal ( University of Portsmouth, UK )'
                ],
                [
                    'time_period' => '2003-2005',
                    'course' => 'GCE A Levels',
                    'institution' => 'Rai International School, New Delhi, India'
                ],
            ],
            'experience' => [
                [
                    'time_period' => '2020-Present',
                    'job_title' => 'Systems Developer',
                    'employer' => 'Reassured'
                ],
                [
                    'time_period' => '2018-2020',
                    'job_title' => 'Full Stack PHP Developer',
                    'employer' => 'Freelance'
                ],
                [
                    'time_period' => '2015-2018',
                    'job_title' => 'PHP Backend Developer',
                    'employer' => 'GIMO'
                ],
                [
                    'time_period' => '2010-2015',
                    'job_title' => 'PHP Developer',
                    'employer' => 'Freelance'
                ],
            ],
            'skills' => [
                [
                    'type' => 'Languages',
                    'values' => 'PHP, HTML, JavaScript, CSS/SASS',
                ],
                [
                    'type' => 'Frameworks / Libraries',
                    'values' => 'Laravel, CakePHP, CodeIgniter, jQuery, Vue.js, Node.js, Bootstrap',
                ],
                [
                    'type' => 'Tools / Platforms',
                    'values' => 'Git, GitLab, AWS, Webpack, Gulp, MySQL, LAMP, LEMP',
                ],
            ],
        ];

        return response()->json($response, 200);
    }

    public function about()
    {
        return response()->json(CmsAbout::all(), 200);
    }
}
