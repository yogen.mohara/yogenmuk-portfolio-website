<?php

namespace App\Http\Controllers;

use App\ContactForm;
use App\Http\Requests\PostContactForm;
use App\Mail\ContactFormMessage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;

class ContactFormController extends Controller
{

    protected $redirectTo = '/#contact-section';
    //
    /**
     * Show the application dashboard.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function contactFormPost(PostContactForm $request)
    {
        /*$this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'subject' => 'required',
            'message' => 'required'
        ]);*/
        ContactForm::create($request->all());

        Mail::send(new ContactFormMessage($request));

        $response = array(
            'status' => 'success',
            'message' => 'Thanks for contacting me! I will get back shortly with your queries.'
        );

        return response()->json($response);
        //return back()->with('success', 'Thanks for contacting me! I will get back shortly with your queries.');

        //$url = route('#contact-section');
        //return redirect()->route('#contact-section')->with('success', 'Thanks for contacting me! I will get back shortly with your queries.');
        //return redirect(route('/') . '#contact-section')->with('success', 'Thanks for contacting me! I will get back shortly with your queries.');
        //$url = URL::route('/', ['#contact-section']);
        //return Redirect::to($url);
    }
}
