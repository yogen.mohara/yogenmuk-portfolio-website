## About

This is my personal portfolio website made with Laravel and Vue.js. 

## Tech Stack Used
- Laravel 6.0
- Vue ^2.6.11
- Vue-axios ^2.1.5
- Vue Router ^3.1.3
- Vuetify 2.1.14
- Backpack 4.0.* (For Admin Panel)

You can check out the demo [here](https://yogenm.uk).
## License

This project is open-source software licensed under the [MIT license](https://opensource.org/licenses/MIT).
