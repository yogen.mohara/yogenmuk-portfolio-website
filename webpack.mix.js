const mix = require('laravel-mix');

mix.webpackConfig({
    resolve: {
        extensions: ['.js', '.vue'],
        alias: {
            '@': __dirname + '/resources'
        }
    }
});

mix.js('resources/js/front/app.js', 'public/assets/js/front/app.js');

mix.sass('resources/sass/front/style.scss', 'public/assets/css/front/style.css')
     .sass('resources/sass/backpack/custom_backpack.scss', 'public/assets/css/backpack/custom_backpack.css')
    .options({
       processCssUrls: false
    });

mix.styles([
    'resources/css/front/vendors/open-iconic-bootstrap.min.css',
    'resources/css/front/vendors/animate.css',
    'resources/css/front/vendors/ionicons.min.css',
    'resources/css/front/vendors/flaticon.css',
    'resources/css/front/vendors/icomoon.css',
], 'public/assets/css/front/vendors.css');

mix.scripts([
    'resources/js/front/vendors/jquery.min.js',
    'resources/js/front/vendors/jquery-migrate-3.0.1.min.js',
    'resources/js/front/vendors/popper.min.js',
    'resources/js/front/vendors/bootstrap.min.js',
    'resources/js/front/vendors/jquery.easing.1.3.js',
], 'public/assets/js/front/vendors.js');

/*
if (mix.inProduction()) {
    mix.version();
}
*/
