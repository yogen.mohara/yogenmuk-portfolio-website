<?php

namespace Database\Seeders;
 
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class CmsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $cms_data = [
            //about
            [
                'category' => 'about',
                'key' => 'name',
                'value' => 'Yogendra Mohara',
            ],
            [
                'category' => 'about',
                'key' => 'job_title',
                'value' => 'Full Stack Web Developer',
            ],
            [
                'category' => 'about',
                'key' => 'intro',
                'value' => 'I am a Full Stack Web Developer, based in the United Kingdom, specializing in developing websites and application with PHP.',
            ],
            [
                'category' => 'about',
                'key' => 'born',
                'value' => '1987',
            ],
            [
                'category' => 'about',
                'key' => 'location',
                'value' => 'Basingstoke, United Kingdom',
            ],
        ];

        foreach($cms_data as $data) {
            DB::table('cms')->insert([
                'category' => $data['category'],
                'key' => $data['key'],
                'value' => $data['value'],
            ]);   
        }
    }
}
