<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

//Route::get('#contact-section', 'HomeController@index');
//Route::get('/', 'HomeController@index');

Route::post('contact-us', 'ContactFormController@contactFormPost' );

Route::get('/{any}', 'FrontSpaController@index')->where('any', '.*');

//Route::get('/download/cv', 'FrontSpaController@cv');

